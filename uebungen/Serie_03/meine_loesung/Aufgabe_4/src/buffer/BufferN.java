package buffer;

import java.util.Random;
import java.util.Scanner;


public class BufferN<T> {

    private T[] things;
    private int numThings = 0;
    private int oldest = -1;
    private int newest = -1;

    public BufferN (int capacity) {

        if (capacity < 1) {
            throw new
                IllegalArgumentException("Nonpositive capacity");
        }
        things = (T[]) new Object[capacity];

    }

    public synchronized T take() {
        System.out.println("take(  ) -> " + this);            
        while(isEmpty()) {
            System.out.println("Buffer empty");
            try {
                wait();
            } catch (InterruptedException e){}
        }
        T tmp = things[oldest++];
        if (oldest >= things.length) {
            oldest = 0;
        }
        numThings--;
        notifyAll();
        return tmp;
    }

    public synchronized void put(T elem) {
        while(numThings >= things.length) {
            System.out.println("Buffer full");
            try {
                wait();
            } catch (InterruptedException e) {}
        }
        if(isEmpty()) {
            things[0] = elem;
            oldest = 0;
            newest = 0;
        }
        else {
            newest++;
            newest = newest % things.length;
            things[newest] = elem;
        }
        /*else if(numThings < things.length) {
            if( newest == things.length-1 ) {
                newest = 0;
                things[0] = elem;
                // invert = true;
            } else {
                things[++newest] = elem;
            }
            }*/
        numThings++;
        notifyAll();
        System.out.println("put(" + elem +") -> " + this);
    }

    public synchronized Boolean isEmpty() {
        if (numThings < 0) {
            System.err.println("numThings = " + numThings);
            numThings = 0;
        }
        return numThings == 0;
   }

    public String toString() {

        String result = "("+numThings+","+oldest+","+newest+"), [";
        for(int i=0; i < things.length; i++) {
            if(i > 0) result += ",";
            if(newest < oldest && numThings > 0) {
                if(i <= newest || i >= oldest) {
                    result += things[i];
                } else result += "_";
            } else if(i >= oldest && i <= newest) {
                result += things[i];
            } else result += "_";
        }
        result += "]";
        return result;

    }

    public static class Consumer extends Thread {
        private static Random rnd = new Random(-42);
        private BufferN<Integer> buf;
        private long sleep;
        
        public Consumer(BufferN<Integer> buf) {
            this.setDaemon(true);
            this.buf = buf;
            this.sleep = (long) (600 + rnd.nextInt(200));
            System.out.println("Consumer with sleep: " + sleep);
        }

        public void run() {
            while(true) {
                buf.take();
                try {
                    Thread.sleep(sleep);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static class Producer extends Thread {
        private static Random rnd = new Random(42);
        private BufferN<Integer> buf;
        private long sleep;
        
        public Producer(BufferN<Integer> buf) {
            this.setDaemon(true);
            this.buf = buf;
            this.sleep = (long) (500 + rnd.nextInt(200));
            System.out.println("Producer with sleep: " + sleep);
        }

        public void run() {
            while(true) {
                int n = rnd.nextInt(20);
                buf.put(n);
                try {
                    Thread.sleep(sleep);
                } catch (InterruptedException e) {
                }
            }
        }
    }
    
    public static void main (String[] args) {

        BufferN<Integer> buf = new BufferN<>(20);

        for(int i = 0; i < 4; i++) new Producer(buf).start();
        for(int i = 0; i < 1; i++) new Consumer(buf).start();

        Scanner s = new Scanner(System.in);
        s.nextInt();
    }

    
}
