package philo;

public class Stick {
    private boolean isUsed;

    public Stick() {
        isUsed = false;
    }

    public synchronized void put() {
        /* EDIT */
        isUsed = false;
        /* EDIT ENDE */
        
        notify();
    }

    public synchronized void take() {
        if (isUsed) {
            try {
                wait();
            } catch (InterruptedException e) { }
        }
        isUsed = true;
    }

    public synchronized boolean isUsed() {
        return isUsed;
    }
}
