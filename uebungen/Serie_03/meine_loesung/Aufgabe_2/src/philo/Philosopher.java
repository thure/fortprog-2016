package philo;

public class Philosopher implements Runnable {
    private Stick left, right;
    private String name;
    public static String lastNameThinking = null;

    public Philosopher(String name, Stick l, Stick r) {
        left = l;
        right = r;
        this.name = name;
    }

    public void philosophize() {
        while (true) {
            if (lastNameThinking != name) {
                lastNameThinking = name;
                System.out.println(name + " is thinking.");
            }
            right.take();
            
            synchronized (left) {
                
                if (! left.isUsed()) {
                    left.take();
                    System.out.println(name + " is eating.");
                    left.put();
                }
            }
            right.put();
        }
    }

    public void run() {
        philosophize();
    }

    public static void main (String[] args) {

        Stick s1 = new Stick();
        Stick s2 = new Stick();
        Stick s3 = new Stick();
        Philosopher p1 = new Philosopher("P1", s1, s2);
        Philosopher p2 = new Philosopher("P2", s2, s3);
        Philosopher p3 = new Philosopher("P3", s3, s1);

        new Thread(p1).start();
        new Thread(p2).start();
       new Thread(p3).start();

    }


}


