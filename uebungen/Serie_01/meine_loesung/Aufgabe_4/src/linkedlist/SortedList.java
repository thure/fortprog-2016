package linkedlist;

import java.util.NoSuchElementException;

import linkedlist.LinkedList;

public class SortedList <T extends Comparable<T>> extends LinkedList<T> {

    public SortedList() {
        super();
    }
    
    public SortedList(T value) {
        super(value);
    }


    public SortedList<T> add(T value) {
        SortedList<T> iter = this;
        while(!iter.isEmpty() && (value.compareTo(iter.getFirst()) >= 0)) {
            final LinkedList<T> tmp = iter.getRest();
            if (tmp instanceof SortedList) {
                iter = (SortedList<T>) tmp;
            }
        }
                
        SortedList<T> t = new SortedList<T> (iter.value);
        t.next = iter.next;
        iter.next = t;
        iter.value = value;
                
        return this;
    }
    
    public static void main(String[] args) {
        System.out.println("Hi!");

        SortedList<Integer> list = new SortedList<>();

        System.out.println(list);

        list.add(0);    System.out.println(list);
        list.add(9);    System.out.println(list);
        list.add(1);    System.out.println(list);
        list.add(8);    System.out.println(list);
        list.add(6);    System.out.println(list);
        list.add(6);    System.out.println(list);
        list.add(3);    System.out.println(list);
        list.add(6);    System.out.println(list);
        list.add(7);    System.out.println(list);
        list.add(4);    System.out.println(list);

        


        
    }

}
