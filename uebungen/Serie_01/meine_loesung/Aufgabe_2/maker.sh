#!/bin/bash

build () {

    [ ! -d bin ] && mkdir bin
    javac -Werror -d bin/ src/linkedlist/LinkedList.java || exit 1
    exit "$?"

}

cleanup () {

    rm -vrf bin
    exit "$?"    

}

run () {
    
    [ -d bin ] || { echo "bin directory not found"; exit 2; }
    cd bin &&
    [ -f 'linkedlist/LinkedList.class' ] || {
        echo "LinkedList class not found";
        exit 3;
    }
    # [ -f 'linkedlist/LinkedList$NoSuchElementException.class' ] || {
    #     echo "NoSuchElementException class not found";
    #     exit 3;
    # }
    java linkedlist/LinkedList
    exit "$?"

}

if [ "$#" -ge 1 ] && [ ! "$1" == 'clean' ] && [ ! "$1" == 'run' ]
then
    echo "Usage: $0         to build"
    echo "Usage: $0 run     to run"    
    echo "Usage: $0 clean   to clean"
    exit 1
fi

if [ "$#" -eq 0 ]
then
    build
fi

case "$1" in
    run)
        run ;;
    clean)
        cleanup ;;
    *)
        echo "Unrecognized parameter"
        exit 2
        ;;
esac
