package optional;

import java.util.NoSuchElementException;

public class Optional<T> {
    
    private T value;
    private boolean present;
    public Optional() {
        present = false;
    }
    public Optional(T v) {
        value = v;
        present = true;
    }
    public boolean isPresent() {
        return present;
    }
    public T get() throws NoSuchElementException {
        if (present) {
            return value;
        }
        throw new NoSuchElementException();
    }

    public static <T> Optional<T> empty() {

        return new Optional<T>();

    }

    public static <T> Optional<T> of(T value) {

        return new Optional<T>(value);

    }
        
    public T orElse (T def) {

        if (isPresent()) {
            return get();
        } else {
            return def;
        }

    }
    
    public static void main (String[] args) {

        prln("Tach auch!");

        Optional<Integer> opt = new Optional<>(new Integer(42));

        try {
            prln(opt.get());
        } catch (NoSuchElementException e) {
            prln("no");
        }
    }

    public static void prln (Object o) {
        System.out.println(o);
    }
}
