#!/bin/bash

build () {

    [ ! -d bin ] && mkdir bin
    javac -Werror -d bin/ src/optional/Optional.java || exit 1
    exit "$?"

}

cleanup () {

    rm -vrf bin
    exit "$?"    

}

run () {
    
    [ -d bin ] || { echo "bin directory not found"; exit 2; }
    cd bin &&
    [ -f 'optional/Optional.class' ] || {
        echo "Optional class not found";
        exit 3;
    }
    # [ -f 'optional/Optional$NoSuchElementException.class' ] || {
    #     echo "NoSuchElementException class not found";
    #     exit 3;
    # }
    java optional/Optional
    exit "$?"

}

if [ "$#" -ge 1 ] && [ ! "$1" == 'clean' ] && [ ! "$1" == 'run' ]
then
    echo "Usage: $0         to build"
    echo "Usage: $0 run     to run"    
    echo "Usage: $0 clean   to clean"
    exit 1
fi

if [ "$#" -eq 0 ]
then
    build
fi

if [ "$#" -eq 1 ] && [ "$1" == 'run' ]
then
    run
fi

if [ "$#" -eq 1 ] && [ "$1" == 'clean' ]
then
    cleanup
fi
