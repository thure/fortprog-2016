package dict;

import dict.LinkedList;

public class Dictionary <K, V> {

    private LinkedList <Pair<K, V>> list = null;
    
    Dictionary () {
        
        // list = new LinkedList <Pair<K, V>> ();
        list = new LinkedList <> ();

    }

    Boolean isEmpty () {

        return list.isEmpty();

    }

    Optional<V> get (K key) {
        LinkedList <Pair <K, V>> iter = list;

        while (iter.isEmpty() == false) {
            final Pair<K, V> pair = iter.getFirst();
            if (pair.first == key) {
                return Optional.of(pair.second);
            }
            iter = iter.getRest();
        }

        // return Optional.empty<V>();
        return Optional.empty();

    }

    Dictionary<K, V> put (K key, V value) {
        LinkedList <Pair <K, V>> iter = list;
        while (iter.isEmpty() == false) {
            final Pair<K, V> pair = iter.getFirst();
            if (pair.first == key) {
                pair.second = value;
                return this;
            }
            iter = iter.getRest();
        }
        list.add (new Pair<>(key, value));
        return this;
    }

    public static void main (String[] args) {

        Dictionary<Integer,String> dict = new Dictionary<>();
        dict.put(7,"Prog").put(8,"2015").put(1,"Fort");
        System.out.print(dict.get(1).orElse(""));
        System.out.print(dict.get(7).orElse(""));
        System.out.print(dict.get(10).orElse("SoSe"));
        System.out.print(dict.get(8).orElse(""));
        System.out.println();

    }



}
