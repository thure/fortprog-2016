package dict;

import java.util.NoSuchElementException;

public class LinkedList <T> {

    private T value;
    private LinkedList<T> next;
        
    public LinkedList() {
        value = null;
        next = null;
    }

    public LinkedList(T value) {
        this.value = value;
        next = null;
    }


    public Boolean isEmpty() {
        
        return value == null && next == null;
    }


    public LinkedList<T> add(T value) {
        LinkedList<T> result = new LinkedList<T>(this.value);
        result.next = this.next;
        this.next = result;
        
        this.value = value;
        
        //LinkedList<T> result = new LinkedList<T>(value);
        //result.next = this;
        return this;
    }

    public Boolean removeNth(Integer n) {
        if (n < 0 || isEmpty())
            return false;

        if (n == 0) {
            this.value = this.next.value;
            this.next = this.next.next;
            return true;
        }

        else {

            if (this.next != null) {
                return this.next.removeNth(n-1);
            } else {
                return false;
            }

        }

        
    }

    public LinkedList<T> getRest() {
        return this.next;
    }

    public T getFirst() {
        return this.value;
    }


    public static void main (String[] args) {

        System.out.println ("Tach auch!");

        LinkedList<Integer> list = new LinkedList<>();
        list.add(7).add(8).add(9)// .add(10).add(11).add(12).add(13).add(14).add(15)
            ;
        // System.out.print(list.getFirst());
        System.out.print(list.removeNth(0));
        while (!list.isEmpty()) {
            System.out.print(list.getFirst());
            // System.out.print(", ");
            list = list.getRest();
        }

    }


}


