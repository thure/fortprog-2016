#!/bin/bash

build () {

    [ ! -d bin ] && mkdir bin
    javac -Werror -d bin/ src/philo/*.java || exit 1
    exit "$?"

}

cleanup () {

    rm -vrf bin
    exit "$?"    

}

run () {
    
    [ -d bin ] || { echo "bin directory not found"; exit 2; }
    cd bin &&
    for c in Philosophers{,2}; do
        [ -f "philo/${c}.class" ] || {
            echo "${c}.class not found";
        }
    done

    if [ "$#" -ne 1 ]; then
        echo run needs a parameter: "1" or "2"
    fi

    b="$1"
    if [ "$b" == "1" ]; then
        java philo/Philosophers
    elif [ "$b" == "2" ]; then
        java philo/Philosophers2
    else
        echo run needs a parameter: "1" or "2"
    fi
    
    exit "$?"

}

if [ "$#" -ge 1 ] && [ ! "$1" == 'clean' ] && [ ! "$1" == 'run' ]
then
    echo "Usage: $0         to build"
    echo "Usage: $0 run     to run"    
    echo "Usage: $0 clean   to clean"
    exit 1
fi

if [ "$#" -eq 0 ]
then
    build
fi

case "$1" in
    run)
        run "$2";;
    clean)
        cleanup ;;
    *)
        echo "Unrecognized parameter"
        exit 2
        ;;
esac
