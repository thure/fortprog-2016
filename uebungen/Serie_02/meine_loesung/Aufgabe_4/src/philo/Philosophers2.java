package philo;

import java.util.concurrent.Semaphore;

public class Philosophers2 implements Runnable {

    private int id;
    private Stick left;
    private Stick right;

    /* Little helper to sleep a period of time */
    private static void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
        }
    }


    public Philosophers2(int id, Stick left, Stick right) {

        this.id = id;
        this.left = left;
        this.right = right;

    }

    public static class Stick {

        private int id;
        private Boolean isTaken = false;

        public int getID () {
            return id;

        }

        public Stick(int id) {
            this.id = id;
        }

        public synchronized Boolean take (Philosophers2 who) {
            if (this.isTaken) {
                return false;
            }
            System.out.println (who.id + " takes " +
                                (who.left == this ?
                                 "left" : "right"));
            this.isTaken = true;
            return true;
        }

        public synchronized void release (Philosophers2 who) {
            this.isTaken = false;
            System.out.println (who.id + " releases " +
                                (who.left == this ?
                                 "left" : "right"));
        }

        public synchronized Boolean isTaken () {
            return this.isTaken;
        }

                

    }

    public void eat() {

        while (!left.take(this)) {
            // do nothing
        }
        

        sleep(50);

        // System.out.println
        //     ("P " + id + " takes left.");

        if (!right.take(this)) {
            left.release(this);
            return;
        }        
        
        System.out.println ("Philosopher "
                            + id + " is now eating."); 
        sleep(20);

        left.release(this);
        // System.out.println
        //     ("P " + id + " releases left.");
        
        right.release(this);
        // System.out.println
        //     ("P " + id + " releases left.");
            
    }

    public void think () {

        //        sleep (25);

        sleep (25 + 50 * id);

    }

    public void run() {

        while (true) {

            think();
            eat();
        }

    }

    public static void main (String[] args) {

        System.out.println("Tach auch!");

        Stick s = new Stick(32);
        Stick t = new Stick(45);

        System.out.println (s.getID());
        System.out.println (t.getID());

        Philosophers2[] philosophers = new Philosophers2[5];
        Stick[] sticks = new Stick[5];

        for (int i = 0; i < 5; i++) {

            sticks[i] = new Stick(i);

        }

        for (int j = 0; j < 5; j++) {

            philosophers[j] = new Philosophers2(j,
                                                sticks[j],
                                                sticks[(j+1) % 5]);

        }

        for (int k = 0; k < 5; k++) {

            new Thread(philosophers[k]).start();

        }
        
    }
    
} 













