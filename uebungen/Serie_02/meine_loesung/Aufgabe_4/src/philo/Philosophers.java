package philo;

import java.util.concurrent.Semaphore;

public class Philosophers implements Runnable {

    private int id;
    private Stick left;
    private Stick right;

    /* Little helper to sleep a period of time */
    private static void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
        }
    }


    public Philosophers(int id, Stick left, Stick right) {

        this.id = id;
        this.left = left;
        this.right = right;

    }

    public static class Stick {

        private int id;
        private Boolean isTaken = false;

        public int getID () {
            return id;

        }

        public Stick(int id) {
            this.id = id;
        }

        public synchronized void take () {
            while (this.isTaken) {
                try {
                    wait();
                } catch  (InterruptedException e) {
                    
                }
            }
            this.isTaken = true;            
        }

        public synchronized void release () {
            this.isTaken = false;
            notify();
        }

        public synchronized Boolean isTaken () {
            return this.isTaken;
        }

                

    }

    public void eat() {

        left.take();
        System.out.println
            ("Stick " + left.getID() +
             " has been taken by philosopher " + id + ".");
        
        right.take();
        System.out.println
            ("Stick " + right.getID() +
             " has been taken by philosopher " + id + ".");

        
        System.out.println ("Philosopher "
                            + id + " is now eating."); 
        sleep(20);

        left.release();
        System.out.println
            ("Stick " + left.getID() + " has been released.");
        
        right.release();
        System.out.println
            ("Stick " + right.getID() + " has been released.");
            
    }

    public void think () {

        sleep (25);

    }

    public void run() {

        while (true) {

            think();
            eat();
        }

    }

    public static void main (String[] args) {

        System.out.println("Tach auch!");

        Stick s = new Stick(32);
        Stick t = new Stick(45);

        System.out.println (s.getID());
        System.out.println (t.getID());

        Philosophers[] philosophers = new Philosophers[5];
        Stick[] sticks = new Stick[5];

        for (int i = 0; i < 5; i++) {

            sticks[i] = new Stick(i);

        }

        for (int j = 0; j < 5; j++) {

            philosophers[j] = new Philosophers(j,
                                               sticks[j],
                                               sticks[(j+1) % 5]);

        }

        for (int k = 0; k < 5; k++) {

            new Thread(philosophers[k]).start();

        }
        
    }
    
} 













