#!/bin/bash

build () {

    [ ! -d bin ] && mkdir bin
    javac -Werror -d bin/ src/counter/*.java || exit 1
    exit "$?"

}

cleanup () {

    rm -vrf bin
    exit "$?"    

}

run () {
    
    [ -d bin ] || { echo "bin directory not found"; exit 2; }
    cd bin &&
    for c in Counter; do
        [ -f "counter/${c}.class" ] || {
            echo "${c}.class not found";
        }
    done

    java counter/Counter
    exit "$?"

}

if [ "$#" -eq 0 ]
then
    build
fi

case "$1" in
    run)
        run ;;
    clean)
        cleanup ;;
    *)
        echo "Unrecognized parameter"
        echo
        echo "Usage: $0         to build"
        echo "Usage: $0 run     to run"    
        echo "Usage: $0 clean   to clean"
        exit 2
        ;;
esac
