package counter;

import java.util.ArrayList;
import java.util.Scanner;

public class Counter implements Runnable {


    public static class Killer extends Thread {

        private int time;

        public Killer (int seconds) {

            time = seconds;
            start();

        }
            
        @Override
        public void run() {
            try {
                Thread.sleep(1000 * time);
            } catch( Exception e) {
                System.err.println("JVM-Killer has been interrupted. Forcing Death!");
                System.exit(-1);
            }
            System.out.println ("Planned death");
            System.exit(0);
        }

    }

    private long value = 0;
    private Boolean running = true;
    private Boolean paused = false;
    private String name = "";
    private long delay = -1;

    public long getValue() {
        return value;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void inc() {
        value = value + 1;
        prln(name + "\t" + value);
    }
    public Counter (long delay, String name) {

        if (delay < 0) {
            prlne ("Negative time has yet to be invented");
            System.exit(1);
        }
        this.delay = delay;
        setName(name);
        Thread t = new Thread (this);
        t.setDaemon(true);
        t.start();
        
        
    }

    /* Little helper to sleep a period of time */
    private static void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
        }
    }


    public void run () {

        while (running) {

            while (running && ! paused) {
            
                sleep(delay);
                inc();
            }
            synchronized (this) {
                try {
                    wait();
                } catch (InterruptedException e) {

                }
            }
        }
    }

    public void stop() {
        running = false;
    }

    public void pause () {
        paused = true;
    }

    public synchronized void unpause () {
        paused = false;
        notifyAll();
    }

        

    public static void main (String[] args) {

        new Killer (5);
            
        prln ("Tach auch!");

        ArrayList<Counter> counters = new ArrayList<>(args.length);

        for (int i = 0; i < args.length; i++) {

            counters.add(new Counter(Integer.parseInt(args[i]), args[i]));

        }

        Scanner s = new Scanner (System.in);
        int n = -1;
        while (n != -2) {
            n = s.nextInt();
            prlne("n = " + n);
            if (n >= 0) {
                if (counters.size() > n) {
                    counters.get(n).stop();
                    
                }
            }
        }
        System.exit(0);
    }










    public static void prln (Object o) {
        System.out.println(o);
    }

    public static void prlne (Object o) {
        System.err.println(o);
    }

}
