package acc;

import java.util.concurrent.Semaphore;

public class Account2 {

    private Integer balance;
    private Semaphore sem;

    public Account2(Integer initialDeposit) {
        balance = initialDeposit;
    }

    public Integer getBalance() {
        
            return balance;
        
    }

    public void deposit(Integer amount) {
        synchronized (this) {
            balance += amount;
        }
    }

    public void withdraw(Integer amount) {
        synchronized (this) {
            deposit(-amount);
        }
    }

    public void transfer(Integer amount, Account2 dest) {
        synchronized (this) {
            synchronized (dest) {
                this.withdraw(amount);
                dest.deposit(amount);
            }
        }
    }

    public synchronized Boolean safeTransfer (Integer amount, Account2 dest) {

        if (getBalance() >= amount) {
            transfer (amount, dest);
            return true;
        } else {
            System.err.println ("Insufficient funds!");
            return false;
        }
    }

    public static void main (String[] args) {

        System.out.println("Tach auch!");

        Account2 a1 = new Account2 (500);
        Account2 a2 = new Account2 (300);

        System.out.println(a1.safeTransfer(100, a2));
        

    }
    
} 













