package acc;

import java.util.concurrent.Semaphore;

public class Account {

    private Integer balance;
    private Semaphore sem;

    public Account(Integer initialDeposit) {
        balance = initialDeposit;
    }

    public Integer getBalance() {
        synchronized (this) {
            return balance;
        }
    }

    public void deposit(Integer amount) {
        synchronized (this) {
            balance += amount;
        }
    }

    public void withdraw(Integer amount) {
        synchronized (this) {
            deposit(-amount);
        }
    }

    public void transfer(Integer amount, Account dest) {
        synchronized (this) {
            synchronized (dest) {
                this.withdraw(amount);
                dest.deposit(amount);
            }
        }
    }

    public synchronized Boolean safeTransfer (Integer amount, Account dest) {

        if (getBalance() >= amount) {
            transfer (amount, dest);
            return true;
        } else {
            System.err.println ("Insufficient funds!");
            return false;
        }
    }

    public static void main (String[] args) {

        System.out.println("Tach auch!");

        Account a1 = new Account (500);
        Account a2 = new Account (300);

        System.out.println(a1.safeTransfer(100, a2));
        

    }
    
} 













