package counter;

import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.BoxLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Container;

public class Counter extends JPanel implements Runnable {

    public static class Killer extends Thread {

        private int seconds;

        public Killer (int seconds) {
            this.seconds = seconds;
            start();
        }
            
        @Override
        public void run() {
            try {
                Thread.sleep(1000 * seconds);
            } catch (Exception e) {
                System.err.println
                    ("JVM-Killer has been interrupted. Forcing Death!");
                System.exit(1);
            }
            System.out.println ("Planned death");
            System.exit(0);
        }
    }

    private abstract class ButtonActionListener implements ActionListener {
        public JButton parent;
        public ButtonActionListener(JButton parent) {
            this.parent = parent;
        }
    }

    private long value = 0;
    private Boolean running = true;
    private Boolean paused = false;
    private String name = "";
    private long delay = -1;
    private JLabel valueLabel = new JLabel();
    private JLabel nameLabel  = new JLabel();
    private JButton pauseButton = new JButton("pause");
    private JButton stopButton = new JButton("stop");

    public void setName (String name) {
        this.name = name;
    }

    public void inc() {
        value = value + 1;
        System.out.println(name + "\t" + value);
    }
    public Counter (long delay, String name) {
        if (delay < 0) {
            throw new IllegalArgumentException
                ("delay mustn't be negative.");
        }

        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        valueLabel = new JLabel();
        nameLabel  = new JLabel();
        pauseButton = new JButton("Pause");

        pauseButton.addActionListener
            (
             new ButtonActionListener(pauseButton) {
                 public void actionPerformed(ActionEvent e) {
                     if (paused) {
                         unpause();
                         parent.setText("Pause");
                     } else {
                         pause();
                         parent.setText("Unpause");
                     }
                 }
             }
             );

        stopButton = new JButton("stop");

        stopButton.addActionListener
            (
             new ButtonActionListener(stopButton) {
                 public void actionPerformed(ActionEvent e) {
                     stop();
                     Container c = parent.getParent();
                     if (c instanceof Counter) {
                         Counter self = (Counter) c;
                         self.pauseButton.setEnabled(false);
                     }
                 }
             }
             );
             
        nameLabel.setText(name + ": ");               

        add(nameLabel);
        add(valueLabel);
        add(pauseButton);
        add(stopButton);
        
        this.delay = delay;
        setName(name);
        Thread t = new Thread (this);
        t.setDaemon(true);
        t.start();
    }

    private static void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
        }
    }

    public void run () {

        while (running) {

            while (running && ! paused) {
            
                sleep(delay);
                inc();
                valueLabel.setText(Long.toString(value));
            }
            synchronized (this) {
                try {
                    wait();
                } catch (InterruptedException e) {
                }
            }
        }
    }

    public void stop() {
        running = false;
    }

    public void pause () {
        paused = true;
    }

    public synchronized void unpause () {
        paused = false;
        notifyAll();
    }

    public static JFrame makeWindow () {

        JFrame window = new JFrame("Zähler");
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setLayout(new BoxLayout(window.getContentPane(),
                                       BoxLayout.Y_AXIS));
        return window;
    }

    public static void main (String[] args) {

        new Killer (60);
            
        JFrame mainFrame = makeWindow();
        mainFrame.setSize(300, 400);

        ArrayList<Counter> counters = new ArrayList<>(args.length);

        for (int i = 0; i < args.length; i++) {

            Counter c = new Counter(Integer.parseInt(args[i]), args[i]);
            counters.add(c);
            mainFrame.add(c);
            c.setVisible(true);
        }
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);
    }
}
